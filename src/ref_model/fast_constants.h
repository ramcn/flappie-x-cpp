#pragma once

constexpr int fast_track_threshold = 5;
constexpr int fast_detect_threshold = 15;
constexpr float fast_track_radius = 5.5f;
constexpr float fast_min_match = 0.2f*0.2f;
constexpr float fast_good_match = 0.65f*0.65f;
constexpr int half_patch_width = 3;
constexpr int full_patch_width = half_patch_width * 2 + 1;
